//
//  ViewController.swift
//  CoturnixQuail
//
//  Created by Bobby Taylor on 3/24/21.
//

import UIKit

class ViewController: UITableViewController {
    var quailData = QuailData()
    
    var pictures = [String]()
    
    //Moved this to QuailData.swift
    /*
    let quailPictures = ["AutumnAmber.jpg","EgyptianMale","FalbFee","GermanPastelCollection","GrauFee","ItalianHen",
                         "JumboWildFemale","JumboWhite","JumboWhiteWingPharaohTop","Panda","Pansy","PansyFee","Pearl",
                         "PharoahMale","Range","Rosetta","RosettaTuxedo","Scarlet_Range","ScarletTuxedo","SchofieldSilver","Snowie","Sparkly","TibetanWithTextFront","TibetanTuxedo"]

    
    let quail = [
        Quail(code: "AutumnAmber", name: "Autumn Amber"),
        Quail(code: "EgyptianMale", name: "Egyptian"),
        Quail(code: "FalbFee", name: "Falb Fee"),
        Quail(code: "GermanPastelCollection", name: "German Pastel"),
        Quail(code: "GrauFee", name: "Grau Fee"),
        Quail(code: "ItalianHen", name: "Italian"),
        Quail(code: "JumboWildFemale", name: "Jumbo Wild"),
        Quail(code: "JumboWhite", name: "Jumbo White"),
        Quail(code: "JumboWhiteWingPharaohTop", name: "Jumbo White Wing Pharaoh"),
        Quail(code: "Panda", name: "Panda"),
        Quail(code: "Pansy", name: "Pansy"),
        Quail(code: "PansyFee", name: "Pansy Fee"),
        Quail(code: "Pearl", name: "Pearl"),
        Quail(code: "PharoahMale", name: "Pharaoh"),
        Quail(code: "Range", name: "Range"),
        Quail(code: "Rosetta", name: "Rosetta"),
        Quail(code: "RosettaTuxedo", name: "Rosetta Tuxedo"),
        Quail(code: "Scarlet_Range", name: "Scarlet / Range"),
        Quail(code: "ScarletTuxedo", name: "Scarlet Tuxedo"),
        Quail(code: "SchofieldSilver", name: "Schofield Silver"),
        Quail(code: "Snowie", name: "Snowie"),
        Quail(code: "Sparkly", name: "Sparkly"),
        Quail(code: "TibetanWithTextFront", name: "Tibetan"),
        Quail(code: "TibetanTuxedo", name: "Tibetan Tuxedo")
    ]
     */
    override func viewDidLoad() {
        
        let backgroundImage = UIImage(named:"Background.jpg")
        let imageView = UIImageView(image: backgroundImage)
        super.viewDidLoad()
        
        title = "My Shire Quail"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        let fm = FileManager.default
        let path = Bundle.main.resourcePath!
        
        let items = try! fm.contentsOfDirectory(atPath: path)
        
        for item in items {
             
            if item.hasSuffix(".jpg"){
                pictures.append(item)
            }
        }//for loop
        
        self.tableView.backgroundView = imageView
        //imageView.contentMode = .scaleAspectFit
        
        //print(quailData.quailNames[0])
        //print(quailData.quailImages)
        
    }//viewDidLoad
    /*
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return pictures.count
     }
     
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
     cell.textLabel?.text = pictures[indexPath.row]
     return cell
     }
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     // 1: try loading the "Detail" view controller and typecasting it to be DetailViewController
     if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
     // 2: success! Set its selectedImage property
     vc.selectedImage = pictures[indexPath.row]
     
     // 3: now push it onto the navigation controller
     navigationController?.pushViewController(vc, animated: true)
     }
     }
     */
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return quail.count
        return quailData.quailNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuailCell", for: indexPath)
        cell.contentView.backgroundColor = UIColor.clear
        
        cell.textLabel?.text = quailData.quailNames[indexPath.row]
        cell.imageView?.image = UIImage(named: quailData.quailImages[indexPath.row])
  
        //print(quailData.quailNames[indexPath.row])
        //let quailLabel = quail[indexPath.row]
        //cell.textLabel?.text = quailLabel.name
        //cell.detailTextLabel?.text = quailLabel.code

        
        
        return cell
    }
    /*
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Quail List"
    }//titleforHeaderSection
    */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // 1: try loading the "Detail" view controller and typecasting it to be DetailViewController
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
            // 2: success! Set its selectedImage property
            vc.selectedImage = quailData.quailImages[indexPath.row]
            //print(indexPath.row)
            //print (quailPictures[indexPath.row])
            
            // 3: now push it onto the navigation controller
            navigationController?.pushViewController(vc, animated: true)
        }
    }//didSelectRowAt
    
}//UITableViewController

//Moved to QuailData.swift struct
/*
struct Quail {
    var code: String
    var name: String
}
 */
