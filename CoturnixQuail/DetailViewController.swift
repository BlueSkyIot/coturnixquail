//
//  DetailViewController.swift
//  CoturnixQuail
//
//  Created by Bobby Taylor on 3/24/21.
//

import UIKit
let quailData = QuailData()

class DetailViewController: UIViewController {
    
    @IBOutlet var quailTitle: UILabel!
    @IBOutlet var quailDetails: UILabel!
    
    @IBOutlet var quailDetailImageOne: UIImageView!
    
    @IBOutlet var quailDetailImageTwo: UIImageView!
    
    
    
    var selectedImage: String?
    //var testImage: String? = "AutumnAmber.jpg"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imageToLoad = selectedImage {
            quailDetailImageOne.image = UIImage(named: imageToLoad)
        }
        
        switch selectedImage {
        case "AutumnAmber.jpg":
            quailTitle.text = quailData.quailNames[0]
            quailDetails.text = quailData.quailDetails[0]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[0])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[1])
        case "EgyptianMale.jpg":
            quailTitle.text = quailData.quailNames[1]
            quailDetails.text = quailData.quailDetails[1]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[2])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[3])
        case "FalbFee.jpg":
            quailTitle.text = quailData.quailNames[2]
            quailDetails.text = quailData.quailDetails[2]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[4])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[5])
        case "GermanPastelCollection.jpg":
            quailTitle.text = quailData.quailNames[3]
            quailDetails.text = quailData.quailDetails[3]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[6])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[7])
        case "GrauFee.jpg":
            quailTitle.text = quailData.quailNames[4]
            quailDetails.text = quailData.quailDetails[4]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[8])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[9])
        case "ItalianHen.jpg":
            quailTitle.text = quailData.quailNames[5]
            quailDetails.text = quailData.quailDetails[5]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[10])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[11])
        case "JumboWildFemale.jpg":
            quailTitle.text = quailData.quailNames[6]
            quailDetails.text = quailData.quailDetails[6]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[18])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[19])
        case "JumboWhite.jpg":
            quailTitle.text = quailData.quailNames[7]
            quailDetails.text = quailData.quailDetails[7]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[14])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[15])
        case "JumboWhiteWingPharaohTop.jpg":
            quailTitle.text = quailData.quailNames[8]
            quailDetails.text = quailData.quailDetails[8]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[16])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[17])
        case "Panda.jpg":
            quailTitle.text = quailData.quailNames[9]
            quailDetails.text = quailData.quailDetails[9]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[20])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[21])
        case "Pansy.jpg":
            quailTitle.text = quailData.quailNames[10]
            quailDetails.text = quailData.quailDetails[10]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[22])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[23])
        case "PansyFee.jpg":
            quailTitle.text = quailData.quailNames[11]
            quailDetails.text = quailData.quailDetails[11]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[24])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[25])
        case "Pearl.jpg":
            quailTitle.text = quailData.quailNames[12]
            quailDetails.text = quailData.quailDetails[12]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[26])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[27])
        case "PharaohMale.jpg":
            quailTitle.text = quailData.quailNames[13]
            quailDetails.text = quailData.quailDetails[13]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[28])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[29])
        case "Range.jpg":
            quailTitle.text = quailData.quailNames[14]
            quailDetails.text = quailData.quailDetails[14]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[30])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[31])
        case "Rosetta.jpg":
            quailTitle.text = quailData.quailNames[15]
            quailDetails.text = quailData.quailDetails[15]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[32])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[33])
        case "RosettaTuxedo.jpg":
            quailTitle.text = quailData.quailNames[16]
            quailDetails.text = quailData.quailDetails[16]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[34])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[35])
        case "Scarlet_Range.jpg":
            quailTitle.text = quailData.quailNames[17]
            quailDetails.text = quailData.quailDetails[17]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[36])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[37])
        case "ScarletTuxedo.jpg":
            quailTitle.text = quailData.quailNames[18]
            quailDetails.text = quailData.quailDetails[18]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[38])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[39])
        case "SchofieldSilver.jpg":
            quailTitle.text = quailData.quailNames[19]
            quailDetails.text = quailData.quailDetails[19]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[40])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[41])
        case "Snowie.jpg":
            quailTitle.text = quailData.quailNames[20]
            quailDetails.text = quailData.quailDetails[20]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[42])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[43])
        case "Sparkly.jpg":
            quailTitle.text = quailData.quailNames[21]
            quailDetails.text = quailData.quailDetails[21]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[44])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[45])
        case "TibetanWithTextFront.jpg":
            quailTitle.text = quailData.quailNames[22]
            quailDetails.text = quailData.quailDetails[22]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[46])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[47])
        case "TibetanTuxedo.jpg":
            quailTitle.text = quailData.quailNames[23]
            quailDetails.text = quailData.quailDetails[23]
            quailDetailImageOne.image = UIImage (named: quailData.quailDetailImages[48])
            quailDetailImageTwo.image = UIImage (named: quailData.quailDetailImages[49])
        default:
            print("Error")
        }
        //quailTitle.text = quailData.quailNames[0]
        //print(quailData.quailNames[1])
        print(selectedImage as Any)
    }//viewDidLoad
    
    @IBAction func onChannelButtonWasPressed(_ sender: Any) {
        
        UIApplication.shared.open(URL(string: "http://myshirefarm.com/" )! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func infoButton(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "https://www.youtube.com/channel/UCqddZmWCunSd07cHR0NteSg/about" )! as URL, options: [:], completionHandler: nil)
    }
    
}
